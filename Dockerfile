FROM adoptopenjdk/openjdk16:alpine-jre
EXPOSE 9000
ADD build/libs/rest-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
