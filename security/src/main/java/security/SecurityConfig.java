package security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import security.secure.FIlterForJwt;

import javax.servlet.http.HttpServletResponse;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

/**
 * Класс кофигурации для модуля Security
 *
 * @author andruha.tm
 */
@Configuration
@ComponentScan
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  /**
   * фильтр jwt
   */
  private final FIlterForJwt fIlterForJwt;

  public SecurityConfig(FIlterForJwt fIlterForJwt) {
    this.fIlterForJwt = fIlterForJwt;
  }

  /**
   * bean енкодера
   * @return
   */
  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  /**
   * метод конфигурации
   * @param http обьект конфигурации
   * @throws Exception ошибка
   */
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
            .httpBasic().disable()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(STATELESS)
            .and()
            .authorizeRequests()
            .antMatchers(POST, "security/login").permitAll()
            .antMatchers(POST, "/customers/").anonymous()
            .antMatchers(DELETE, "/customers/").authenticated()
            .antMatchers(DELETE, "/goods/").authenticated()
            .antMatchers(DELETE, "/customers/").authenticated()
            .antMatchers(GET, "/goods/").permitAll()
            .antMatchers(GET, "/customers").authenticated()
            .antMatchers(GET, "/inventory").permitAll()
            .antMatchers(GET, "/orders").permitAll()
            .antMatchers(POST, "/goods/").authenticated()
            .antMatchers(POST, "/inventory/").authenticated()
            .antMatchers(PUT, "/goods/").authenticated()
            .antMatchers(PUT, "/inventory/").authenticated()
            .antMatchers("/api/").permitAll()

            // Добавить новые методы
            .and()
            .addFilterBefore(fIlterForJwt, UsernamePasswordAuthenticationFilter.class)
            .logout()
            .logoutUrl("/security/logout")
            .permitAll()
            .logoutSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> httpServletResponse.setStatus(HttpServletResponse.SC_OK));

  }
}
