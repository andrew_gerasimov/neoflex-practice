CREATE TABLE "customers"
(
    "id"       uuid         NOT NULL,
    "name"     varchar(70)  NOT NULL,
    "surname"  varchar(70)  NOT NULL,
    "email"    varchar(70)  NOT NULL,
    "phone"    varchar(70)  NOT NULL,
    "password" varchar(256) NOT NULL,
    CONSTRAINT "customers_pk" PRIMARY KEY ("id")
) WITH (
      OIDS= FALSE
    );

CREATE TABLE "goods"
(
    "product_id"  uuid             NOT NULL,
    "name"        varchar(70)      NOT NULL,
    "description" varchar(250)     NOT NULL,
    "price"       double precision NOT NULL,
    "warranty"    TIMESTAMP        NOT NULL,
    CONSTRAINT "goods_pk" PRIMARY KEY ("product_id")
) WITH (
      OIDS= FALSE
    );

CREATE TABLE "inventories"
(
    "product_id" uuid NOT NULL,
    "available"  int  NOT NULL,
    CONSTRAINT "inventories_pk" PRIMARY KEY ("product_id")
) WITH (
      OIDS= FALSE
    );

CREATE TABLE "orders"
(
    "order_id"     uuid NOT NULL,
    "inventory_id" uuid NOT NULL,
    "customer_id"  uuid NOT NULL,
    "count"        int  NOT NULL,
    CONSTRAINT "orders_pk" PRIMARY KEY ("order_id")
) WITH (
      OIDS= FALSE
    );


ALTER TABLE "inventories"
    ADD CONSTRAINT "inventorties_fk0" FOREIGN KEY ("product_id") REFERENCES "goods" ("product_id");

ALTER TABLE "orders"
    ADD CONSTRAINT "orders_fk0" FOREIGN KEY ("inventory_id") REFERENCES "inventories" ("product_id");
ALTER TABLE "orders"
    ADD CONSTRAINT "orders_fk1" FOREIGN KEY ("customer_id") REFERENCES "customers" ("id");
