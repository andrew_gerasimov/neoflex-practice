package com.practice.rest.api;

import com.practice.services.dto.OrderDto;
import com.practice.services.service.IOrderService;
import com.practice.services.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(
				value = "/orders",
				produces = MediaType.APPLICATION_JSON_VALUE
)
public class OrderController {

	private final IOrderService orderService;

	@Autowired
	public OrderController(IOrderService orderService) {
		this.orderService = orderService;
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderDto> createOrder(@RequestBody OrderDto orderDto) throws EntityNotFoundException {
		return new ResponseEntity<>(orderService.create(orderDto), HttpStatus.CREATED);
	}

	/**
	 * updating order info mthd
	 * @param orderId id of order to update
	 * @param orderDto order info
	 * @return update order entity info
	 */
	@PutMapping(value = "/{id}")
	public OrderDto update(
					@PathVariable("id") UUID orderId,
					@RequestBody OrderDto orderDto
	) throws EntityNotFoundException {
		orderDto.setId(orderId);
		return orderService.update(orderDto);
	}

	/**
	 * retrieving order entity info mthd
	 * @param orderId id of entity get
	 * @return order entity info
	 */
	@GetMapping(value = "/{id}")
	public OrderDto getById(@PathVariable("id") UUID orderId) throws EntityNotFoundException {
		return orderService.getById(orderId);
	}

	/**
	 * deleting order entity mthd
	 * @param orderId order id to delete
	 */
	@DeleteMapping(value = "/{id}")
	public void deleteById(@PathVariable("id") UUID orderId) throws EntityNotFoundException {
		orderService.delete(orderId);
	}
}
