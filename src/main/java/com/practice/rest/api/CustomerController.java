package com.practice.rest.api;

import com.practice.services.dto.CustomerDto;
import com.practice.services.service.ICustomerService;
import com.practice.services.service.exception.EmailAlreadyInUseException;
import com.practice.services.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(
				path = "/customers",
				produces = MediaType.APPLICATION_JSON_VALUE
)
public class CustomerController {

	private final ICustomerService customerService;

	@Autowired
	public CustomerController(ICustomerService customerService) {
		this.customerService = customerService;
	}

	/**
	 * creating customer entity mthd
	 * @param customerDto customer info
	 * @return saved customer entity info
	 * @throws EmailAlreadyInUseException if passed customer email is already in use
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CustomerDto> createCustomer(@RequestBody CustomerDto customerDto) throws EmailAlreadyInUseException, EntityNotFoundException {
		return new ResponseEntity<>(customerService.create(customerDto), HttpStatus.CREATED);
	}

	/**
	 * retrieving customer info mthd
	 * @param customerId id of entity to get
	 * @return customer info
	 */
	@GetMapping(value = "/{id}")
	public CustomerDto getCustomerById(@PathVariable("id") UUID customerId) throws EntityNotFoundException {
		return customerService.getByID(customerId);
	}

	/**
	 * updating customer entity mthd
	 * @param customerId id of entity to update
	 * @param customerDto info to update
	 * @return updated entity info
	 */
	@PutMapping(value = "/{id}")
	public CustomerDto update(
					@PathVariable("id") UUID customerId,
					@RequestBody CustomerDto customerDto
	) throws EntityNotFoundException {
		customerDto.setId(customerId);
		return customerService.update(customerDto);
	}

	/**
	 * deleting cusotmer entity mthd
	 * @param customerId id of entity to delete
	 */
	@DeleteMapping(value = "/{id}")
	public void deleteById(@PathVariable("id") UUID customerId) throws EntityNotFoundException {
		customerService.remove(customerId);
	}


}
