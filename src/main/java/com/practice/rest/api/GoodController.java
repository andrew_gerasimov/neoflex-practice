package com.practice.rest.api;

import com.practice.services.dto.GoodDto;
import com.practice.services.service.IGoodService;
import com.practice.services.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(
				value = "/goods",
				produces = MediaType.APPLICATION_JSON_VALUE
)
public class GoodController {

	private final IGoodService goodService;

	@Autowired
	public GoodController(IGoodService goodService) {
		this.goodService = goodService;
	}

	/**
	 * creating good entity mthd
	 * @param goodDto good info
	 * @return created good entity info
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public GoodDto createGood(@RequestBody GoodDto goodDto) throws EntityNotFoundException {
		return goodService.create(goodDto);
	}

	/**
	 * retrieving good info mthd
	 * @param goodId id of entity to get
	 * @return good entity info
	 */
	@GetMapping(value = "/{id}")
	public GoodDto getById(@PathVariable("id") UUID goodId) throws EntityNotFoundException {
		return goodService.getById(goodId);
	}

	/**
	 * updating good entity info mthd
	 * @param goodId id of entity to get
	 * @param goodDto info to update
	 * @return updated entity info
	 */
	@PatchMapping(value = "/{id}")
	public GoodDto update(
					@PathVariable("id") UUID goodId,
					@RequestBody GoodDto goodDto
	) throws EntityNotFoundException {
		goodDto.setId(goodId);
		return goodService.update(goodDto);
	}

	/**
	 * deleting good entity mthd
	 * @param goodId id of entity to delete
	 */
	@DeleteMapping(value = "/{id}")
	public void deleteById(@PathVariable("id") UUID goodId) throws EntityNotFoundException {
		goodService.deleteById(goodId);
	}
}
