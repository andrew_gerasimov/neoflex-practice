package com.practice.rest.api;

import com.practice.services.dto.InventoryDto;
import com.practice.services.service.IInventoryService;
import com.practice.services.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(
				value = "/inventory",
				produces = MediaType.APPLICATION_JSON_VALUE
)
public class InventoryController {

	private final IInventoryService inventoryService;

	@Autowired
	public InventoryController(IInventoryService inventoryService) {
		this.inventoryService = inventoryService;
	}

	/**
	 * creating inventory item mthd
	 * @param inventoryDto dto model of inventory
	 * @return saved inventory entity
	 * @throws EntityNotFoundException when referencing {@link com.practice.data.models.Good doesn't exist}
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InventoryDto> createInventory(@RequestBody InventoryDto inventoryDto) throws EntityNotFoundException {
		return new ResponseEntity<>(inventoryService.create(inventoryDto), HttpStatus.CREATED);
	}

	/**
	 * retrieving inventory item info mthd
	 * @param inventoryId id of inventory item
	 * @return inventory item info
	 */
	@GetMapping(value = "/{id}")
	public InventoryDto getById(@PathVariable("id") UUID inventoryId) throws EntityNotFoundException {
		return inventoryService.getById(inventoryId);
	}

	/**
	 * updating inventory item info mthd
	 * @param inventoryId id of item to update
	 * @param inventoryDto item data to update
	 * @return updated item info
	 */
	@PutMapping(value = "/{id}")
	public InventoryDto update(
					@PathVariable("id") UUID inventoryId,
					@RequestBody InventoryDto inventoryDto
	) throws EntityNotFoundException {
		inventoryDto.setId(inventoryId);
		return inventoryService.update(inventoryDto);
	}

	/**
	 * deleting inventory item mthd
	 * @param inventoryId id of item to delete
	 */
	@DeleteMapping(value = "/{id}")
	public void deleteById(@PathVariable("id") UUID inventoryId) throws EntityNotFoundException {
		inventoryService.deleteById(inventoryId);
	}
}
