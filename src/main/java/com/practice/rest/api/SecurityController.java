package com.practice.rest.api;

import com.practice.services.dto.AuthInfoDto;
import com.practice.services.dto.CustomerDto;
import com.practice.services.service.ICustomerService;
import com.practice.services.service.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/security")
@Slf4j
public class SecurityController {

    private static Logger logger = LoggerFactory.getLogger(SecurityController.class);

    private ICustomerService userService;

    @Autowired
    public SecurityController(ICustomerService userService) {
        this.userService = userService;
    }

    /**
     * метод Аутентификация  для пользователя
     * @param loginUserDto - dto для выхода
     * @return AuthInfoDto - где хранится токен для пользователя
     */
    @RequestMapping(method = POST, path = "/login")
    public ResponseEntity<AuthInfoDto> loginUser(@RequestBody CustomerDto loginUserDto) throws EntityNotFoundException {
        AuthInfoDto authInfoDto = userService.loginUser(loginUserDto);
        return authInfoDto != null
                ? new ResponseEntity<AuthInfoDto>(authInfoDto, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
