package com.practice.services;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({
				"com.practice.services.mapstruct",
				"com.practice.services.service"
})
public class ServicesConfiguration {
}

