package com.practice.services.service.impl;

import com.practice.data.models.Customer;
import com.practice.data.repository.CustomerRepository;
import com.practice.services.dto.AuthInfoDto;
import com.practice.services.dto.CustomerDto;
import com.practice.services.mapstruct.CustomerStruct;
import com.practice.services.service.ICustomerService;
import com.practice.services.service.exception.EmailAlreadyInUseException;
import com.practice.services.service.exception.EntityNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import security.secure.JwtSupplier;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class CustomerServiceImpl implements ICustomerService {

	private final PasswordEncoder passwordEncoder;

	private final JwtSupplier jwtSupplier;

	private static final Logger logger = Logger.getLogger(CustomerServiceImpl.class);

	private final CustomerRepository customerRepository;

	private final CustomerStruct customerStruct;

	@Autowired
	public CustomerServiceImpl(CustomerRepository customerRepository, CustomerStruct customerStruct, PasswordEncoder passwordEncoder, JwtSupplier jwtSupplier) {
		this.jwtSupplier = jwtSupplier;
		this.passwordEncoder = passwordEncoder;
		this.customerRepository = customerRepository;
		this.customerStruct = customerStruct;
	}

	@Override
	@Transactional
	public CustomerDto create(CustomerDto customerDto) throws EmailAlreadyInUseException, EntityNotFoundException {
		var password = passwordEncoder.encode(customerDto.getPassword());
		customerDto.setPassword(password);
		Customer customer = customerStruct.fromDto(customerDto);
		if (customerRepository.findByEmail(customerDto.getEmail()) != null) {
			logger.info("UNSUCCESSFUL");
			throw new EmailAlreadyInUseException(customerDto.getEmail());
		}
		Customer newCustomer = customerRepository.save(customer);
		logger.info("SUCCESFULLY CREATED CUSTOMER");
		return toCustomerDto(newCustomer);
	}

	@Override
	@Transactional
	public CustomerDto update(CustomerDto customerDto) throws EntityNotFoundException {
		if (customerRepository.findById(customerDto.getId()).isEmpty()) {
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		Customer customer = customerRepository.getOne(customerDto.getId());
		logger.info(customer.toString());
		logger.info("SUCCESFULLY UPDATED CUSTOMER");
		return customerStruct.toDto(customerRepository.save(customerStruct.updateFromDto(customerDto, customer)));
	}

	@Override
	@Transactional
	public void remove(UUID customerId) throws EntityNotFoundException {
		if (customerRepository.findById(customerId).isEmpty()) {
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		customerRepository.deleteById(customerId);
		logger.info("SUCCESFULLY DELETED CUSTOMER");
	}

	@Override
	public CustomerDto getByID(UUID customerId) throws EntityNotFoundException {
		if (customerRepository.findById(customerId).isEmpty()) {
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		return customerStruct.toDto(customerRepository.getOne(customerId));
	}

	@Override
	public List<CustomerDto> getCustomers() {
		return customerStruct.toDto(customerRepository.findAll());
	}

	@Override
	public AuthInfoDto loginUser(CustomerDto customerDto) throws EntityNotFoundException {
		var user = customerRepository.findByEmail(customerDto.getEmail());
		if (user == null)
			throw new EntityNotFoundException(customerDto.getEmail());
		if (passwordEncoder.matches(customerDto.getPassword(), user.getPassword())) {
			return generateTokenFromUser(user);
		}
		return null;
	}

	public AuthInfoDto generateTokenFromUser(Customer customer) {
		return new AuthInfoDto(jwtSupplier.createTokenForUser(customer.getName()), customer.getCustomer_id());
	}

	public CustomerDto toCustomerDto(Customer customer) {
		return customerStruct.toDto(customer);
	}

}
