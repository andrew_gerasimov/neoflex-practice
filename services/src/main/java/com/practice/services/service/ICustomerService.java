package com.practice.services.service;


import com.practice.services.dto.AuthInfoDto;
import com.practice.services.dto.CustomerDto;
import com.practice.services.service.exception.EmailAlreadyInUseException;
import com.practice.services.service.exception.EntityNotFoundException;

import java.util.List;
import java.util.UUID;

public interface ICustomerService {

	CustomerDto create(CustomerDto customerDto) throws EmailAlreadyInUseException, EntityNotFoundException;

	CustomerDto update(CustomerDto customerDto) throws EntityNotFoundException;

	CustomerDto getByID(UUID customerId) throws EntityNotFoundException;

	void remove(UUID customerId) throws EntityNotFoundException;

	List<CustomerDto> getCustomers();

	AuthInfoDto loginUser(CustomerDto customerDto) throws EntityNotFoundException;
}
