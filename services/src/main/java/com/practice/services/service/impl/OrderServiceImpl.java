package com.practice.services.service.impl;

import com.practice.data.models.Customer;
import com.practice.data.models.Inventory;
import com.practice.data.models.Order;
import com.practice.data.repository.CustomerRepository;
import com.practice.data.repository.InventoryRepository;
import com.practice.data.repository.OrderRepository;
import com.practice.services.dto.OrderDto;
import com.practice.services.mapstruct.CustomerStruct;
import com.practice.services.mapstruct.OrderStruct;
import com.practice.services.service.IOrderService;
import com.practice.services.service.exception.EntityNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class OrderServiceImpl implements IOrderService {

	private static final Logger logger = Logger.getLogger(OrderServiceImpl.class);

	private final OrderRepository orderRepository;

	private final CustomerRepository customerRepository;

	private final InventoryRepository inventoryRepository;

	private final OrderStruct orderStruct;

	private final CustomerStruct customerStruct;

	@Autowired
	public OrderServiceImpl(OrderRepository orderRepository, CustomerRepository customerRepository, InventoryRepository inventoryRepository, OrderStruct orderStruct, CustomerStruct customerStruct) {
		this.orderRepository = orderRepository;
		this.customerRepository = customerRepository;
		this.inventoryRepository = inventoryRepository;
		this.orderStruct = orderStruct;
		this.customerStruct = customerStruct;
	}

	@Override
	@Transactional
	public OrderDto create(OrderDto orderDto) throws EntityNotFoundException {
		if (customerRepository.findByEmail(orderDto.getCustomer().getEmail()) == null) {
			logger.info("CUSTOMER ENTITY DOESN'T EXIST");
			throw new EntityNotFoundException("CUSTOMER ENTITY DOESN'T EXIST");
		}
		if (
						inventoryRepository.findByProductid(orderDto.getInventory().getId()) != null
										&& inventoryRepository.findByProductid(orderDto.getInventory().getId()).getAmount() >= orderDto.getCount()
		) {

			//save contract
			Order order = orderStruct.fromDto(orderDto);

			//set Customer and Inventory items
			Customer customer = customerRepository.findByEmail(orderDto.getCustomer().getEmail());
			Inventory inventory = inventoryRepository.getOne(orderDto.getInventory().getId());
			order.setCustomer(customer);
			order.setInventory(inventory);
//			orderDto.setCustomer(customerStruct.toDto(customer));

			//change inventory items
			Inventory editable = inventoryRepository.findByProductid(orderDto.getInventory().getId());
			editable.setAmount(editable.getAmount() - orderDto.getCount());
			inventoryRepository.save(editable);

			Order newOrder = orderRepository.save(order);
			logger.info("SUCCESFULLY CREATED GOOD");

			//mail to client
			return orderStruct.toDto(newOrder);
		} else {
			logger.info("INVENTORY ITEM NOT FOUND OR HAVE LESS ITEMS THEN REQUIRED");
			throw new EntityNotFoundException("INVENTORY ITEM NOT FOUND OR HAVE LESS ITEMS THEN REQUIRED");
		}
	}

	@Override
	@Transactional
	public OrderDto update(OrderDto orderDto) throws EntityNotFoundException {
		if (orderRepository.findById(orderDto.getId()).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		Order order = orderRepository.getOne(orderDto.getId());
		logger.info("SUCCESFULLY UPDATED ORDER");
		return orderStruct.toDto(orderRepository.save(orderStruct.updateFromDto(orderDto,order)));
	}

	@Override
	public OrderDto getById(UUID orderId) throws EntityNotFoundException {
		if (orderRepository.findById(orderId).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		return orderStruct.toDto(orderRepository.getOne(orderId));
	}

	@Override
	@Transactional
	public void delete(UUID orderId) throws EntityNotFoundException {
		if (orderRepository.findById(orderId).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		orderRepository.deleteById(orderId);
		logger.info("SUCCESFULLY DELETED ORDER");
	}

	@Override
	public List<OrderDto> getOrders() {
		return orderStruct.toDto(orderRepository.findAll());
	}
}
