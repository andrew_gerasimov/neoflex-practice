package com.practice.services.service;

import com.practice.services.dto.InventoryDto;
import com.practice.services.service.exception.EntityNotFoundException;

import java.util.List;
import java.util.UUID;

public interface IInventoryService {

	InventoryDto create(InventoryDto inventoryDto) throws EntityNotFoundException;

	InventoryDto update(InventoryDto inventoryDto) throws EntityNotFoundException;

	InventoryDto getById(UUID inventoryId) throws EntityNotFoundException;

	void deleteById(UUID inventoryId) throws EntityNotFoundException;

	List<InventoryDto> getInventories();
}
