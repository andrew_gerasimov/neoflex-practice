package com.practice.services.service.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class EmailAlreadyInUseException extends Exception{
	private String email;

	public EmailAlreadyInUseException(String email) {
		super();
		this.email = email;
	}

	public EmailAlreadyInUseException(String message, String email) {
		super(message);
		this.email = email;
	}
}
