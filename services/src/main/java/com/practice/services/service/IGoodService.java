package com.practice.services.service;

import com.practice.services.dto.GoodDto;
import com.practice.services.service.exception.EntityNotFoundException;

import java.util.List;
import java.util.UUID;

public interface IGoodService {

	GoodDto create(GoodDto goodDto) throws EntityNotFoundException;

	GoodDto update(GoodDto goodDto) throws EntityNotFoundException;

	GoodDto getById(UUID goodId) throws EntityNotFoundException;

	void deleteById(UUID goodId) throws EntityNotFoundException;

	List<GoodDto> getGoods();

}
