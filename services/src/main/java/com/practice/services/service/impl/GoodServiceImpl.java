package com.practice.services.service.impl;

import com.practice.data.models.Good;
import com.practice.data.repository.GoodRepository;
import com.practice.services.dto.GoodDto;
import com.practice.services.mapstruct.GoodStruct;
import com.practice.services.service.IGoodService;
import com.practice.services.service.exception.EntityNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class GoodServiceImpl implements IGoodService {

	private static final Logger logger = Logger.getLogger(GoodServiceImpl.class);

	private final GoodRepository goodRepository;

	private final GoodStruct goodStruct;

	@Autowired
	public GoodServiceImpl(GoodRepository goodRepository, GoodStruct goodStruct) {
		this.goodRepository = goodRepository;
		this.goodStruct = goodStruct;
	}

	@Override
	@Transactional
	public GoodDto create(GoodDto goodDto) throws EntityNotFoundException {
		Good good = goodStruct.fromDto(goodDto);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//		good.setWarranty(good.getWarranty() + timestamp);
		Good newGood = goodRepository.save(good);
		logger.info("SUCCESFULLY CREATED GOOD");
		return goodStruct.toDto(newGood);
	}

	@Override
	@Transactional
	public GoodDto update(GoodDto goodDto) throws EntityNotFoundException {
		if (goodRepository.findById(goodDto.getId()).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		Good good = goodRepository.getOne(goodDto.getId());
		logger.info("SUCCESFULLY UPDATED GOOD");
		return goodStruct.toDto(goodRepository.save(goodStruct.updateFromDto(goodDto,good)));
	}

	@Override
	public GoodDto getById(UUID goodId) throws EntityNotFoundException {
		if (goodRepository.findById(goodId).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		return goodStruct.toDto(goodRepository.getOne(goodId));
	}

	@Override
	@Transactional
	public void deleteById(UUID goodId) throws EntityNotFoundException {
		if (goodRepository.findById(goodId).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		goodRepository.deleteById(goodId);
		logger.info("SUCCESFULLY DELETED GOOD");
	}

	@Override
	public List<GoodDto> getGoods() {
		return goodStruct.toDto(goodRepository.findAll());
	}
}
