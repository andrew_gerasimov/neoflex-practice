package com.practice.services.service.impl;

import com.practice.data.models.Inventory;
import com.practice.data.repository.GoodRepository;
import com.practice.data.repository.InventoryRepository;
import com.practice.services.dto.InventoryDto;
import com.practice.services.mapstruct.InventoryStruct;
import com.practice.services.service.IInventoryService;
import com.practice.services.service.exception.EntityNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class InventoryServiceImpl implements IInventoryService {

	private static final Logger logger = Logger.getLogger(InventoryServiceImpl.class);

	private final InventoryRepository inventoryRepository;

	private final GoodRepository goodRepository;

	private final InventoryStruct inventoryStruct;

	@Autowired
	public InventoryServiceImpl(InventoryRepository inventoryRepository, InventoryStruct inventoryStruct, GoodRepository goodRepository) {
		this.goodRepository = goodRepository;
		this.inventoryRepository = inventoryRepository;
		this.inventoryStruct = inventoryStruct;
	}

	@Override
	@Transactional
	public InventoryDto create(InventoryDto inventoryDto) throws EntityNotFoundException {
		if (inventoryRepository.findById(inventoryDto.getId()).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		Inventory inventory = inventoryStruct.fromDto(inventoryDto);
		if (goodRepository.findByNameAndID(inventoryDto.getId()) == null){
			logger.info("UNSUCCESSFUL");
			throw new EntityNotFoundException(inventoryDto.getId().toString());
		}
		inventory.setGood(goodRepository.getOne(inventoryDto.getId()));
		Inventory newInventory = inventoryRepository.save(inventory);
		logger.info("SUCCESFULLY CREATED INVENTORY");
		return inventoryStruct.toDto(newInventory);
	}

	@Override
	@Transactional
	public InventoryDto update(InventoryDto inventoryDto) throws EntityNotFoundException {
		if (inventoryRepository.findById(inventoryDto.getId()).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		Inventory inventory = inventoryRepository.getOne(inventoryDto.getId());
		logger.info("SUCCESFULLY UPDATED INVENTORY");
		return inventoryStruct.toDto(inventoryRepository.save(inventoryStruct.updateFromDto(inventoryDto,inventory)));
	}

	@Override
	public InventoryDto getById(UUID inventoryId) throws EntityNotFoundException {
		if (inventoryRepository.findById(inventoryId).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		return inventoryStruct.toDto(inventoryRepository.getOne(inventoryId));
	}

	@Override
	@Transactional
	public void deleteById(UUID inventoryId) throws EntityNotFoundException {
		if (inventoryRepository.findById(inventoryId).isEmpty()){
			throw new EntityNotFoundException("NO SUCH UUID");
		}
		inventoryRepository.deleteById(inventoryId);
		logger.info("SUCCESFULLY DELETED INVENTORY");
	}

	@Override
	public List<InventoryDto> getInventories() {
		return inventoryStruct.toDto(inventoryRepository.findAll());
	}
}
