package com.practice.services.service;

import com.practice.services.dto.OrderDto;
import com.practice.services.service.exception.EntityNotFoundException;

import java.util.List;
import java.util.UUID;

public interface IOrderService {

	OrderDto create(OrderDto orderDto) throws EntityNotFoundException;

	OrderDto update(OrderDto orderDto) throws EntityNotFoundException;

	OrderDto getById(UUID orderId) throws EntityNotFoundException;

	void delete(UUID orderId) throws EntityNotFoundException;

	List<OrderDto> getOrders();
}
