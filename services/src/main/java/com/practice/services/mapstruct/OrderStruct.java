package com.practice.services.mapstruct;

import com.practice.data.models.Good;
import com.practice.data.models.Order;
import com.practice.services.dto.GoodDto;
import com.practice.services.dto.OrderDto;
import com.practice.services.mapstruct.config.BaseMapperConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = BaseMapperConfig.class)
public interface OrderStruct {

	@Mapping(target = "id",source = "order_id")
	@Mapping(target = "inventory.id", source = "inventory.product_id")
	@Mapping(target = "customer.id", source = "customer.customer_id")
	OrderDto toDto(Order order);

	@Mapping(target = "order_id",source = "id")
	Order fromDto(OrderDto order);

	@Mapping(target = "order_id", ignore = true)
	Order updateFromDto(OrderDto orderDto, @MappingTarget Order order);

	List<OrderDto> toDto(List<Order> orderList);

	List<Order> fromDto(List<OrderDto> orderList);
}
