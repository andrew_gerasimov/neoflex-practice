package com.practice.services.mapstruct;

import com.practice.data.models.Good;
import com.practice.services.dto.GoodDto;
import com.practice.services.mapstruct.config.BaseMapperConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = BaseMapperConfig.class)
public interface GoodStruct {

	@Mapping(target = "id", source = "product_id")
	GoodDto toDto(Good good);

	@Mapping(target = "product_id", source = "id")
	Good fromDto(GoodDto good);

	@Mapping(target = "product_id", ignore = true)
	Good updateFromDto(GoodDto goodDto, @MappingTarget Good good);

	List<GoodDto> toDto(List<Good> goods);

	List<Good> fromDto(List<GoodDto> goods);
}
