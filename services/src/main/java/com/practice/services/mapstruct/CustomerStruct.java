package com.practice.services.mapstruct;

import com.practice.data.models.Customer;
import com.practice.services.dto.CustomerDto;
import com.practice.services.mapstruct.config.BaseMapperConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = BaseMapperConfig.class)
public interface CustomerStruct {

	@Mapping(source = "customer_id", target = "id")
	CustomerDto toDto(Customer customer);

	@Mapping(source = "id", target = "customer_id")
	Customer fromDto(CustomerDto customer);

	@Mapping(target = "customer_id", ignore = true)
	Customer updateFromDto(CustomerDto customerDto, @MappingTarget Customer customer);

	List<CustomerDto> toDto(List<Customer> customers);

	List<CustomerDto> fromDto(List<Customer> customers);
}
