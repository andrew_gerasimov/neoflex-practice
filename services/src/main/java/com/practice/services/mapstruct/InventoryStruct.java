package com.practice.services.mapstruct;

import com.practice.data.models.Inventory;
import com.practice.data.models.Order;
import com.practice.services.dto.InventoryDto;
import com.practice.services.dto.OrderDto;
import com.practice.services.mapstruct.config.BaseMapperConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = BaseMapperConfig.class)
public interface InventoryStruct {

	@Mapping(target = "id", source = "product_id")
	@Mapping(target = "good.id", source = "good.product_id")
	InventoryDto toDto(Inventory inventory);

	@Mapping(target = "product_id", source = "id")
	Inventory fromDto(InventoryDto inventory);

	@Mapping(target = "product_id", ignore = true)
	Inventory updateFromDto(InventoryDto inventoryDto, @MappingTarget Inventory inventory);

	List<InventoryDto> toDto(List<Inventory> inventories);

	List<Inventory> fromDto(List<InventoryDto> inventoryDtos);
}
