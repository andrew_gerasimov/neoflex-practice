package com.practice.services.dto;

import java.util.List;
import java.util.UUID;

public class InventoryDto {

	private UUID id;
	private GoodDto good;
	private Integer amount;
//	private List<OrderDto> orderList;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public GoodDto getGood() {
		return good;
	}

	public void setGood(GoodDto good) {
		this.good = good;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

//	public List<OrderDto> getOrderList() {
//		return orderList;
//	}
//
//	public void setOrderList(List<OrderDto> orderList) {
//		this.orderList = orderList;
//	}
}
