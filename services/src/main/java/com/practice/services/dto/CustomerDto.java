package com.practice.services.dto;

import java.util.List;
import java.util.UUID;

public class CustomerDto {

	private UUID id;
	private String name;
	private String surname;
	private String email;
	private String phone;
	private String password;
//	private List<OrderDto> orders;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

//	public List<OrderDto> getOrders() {
//		return orders;
//	}
//
//	public void setOrders(List<OrderDto> orders) {
//		this.orders = orders;
//	}
}
