package com.practice.data;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan
@EntityScan(basePackages = "com.practice.data.models")
@EnableJpaRepositories(basePackages = "com.practice.data.repository")
public class DataConfiguration {
}
