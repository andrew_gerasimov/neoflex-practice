package com.practice.data.repository;

import com.practice.data.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CustomerRepository extends JpaRepository<Customer, UUID> {

	Customer findByEmail(String email);

}
