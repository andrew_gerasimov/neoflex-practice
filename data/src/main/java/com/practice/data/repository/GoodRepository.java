package com.practice.data.repository;

import com.practice.data.models.Good;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface GoodRepository extends JpaRepository<Good, UUID> {
	@Query(value = "select * from goods i where product_id = :uuid ", nativeQuery = true)
	Good findByNameAndID(@Param("uuid") UUID uuid);
}
