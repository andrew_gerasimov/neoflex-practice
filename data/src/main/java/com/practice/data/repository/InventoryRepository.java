package com.practice.data.repository;

import com.practice.data.models.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface InventoryRepository extends JpaRepository<Inventory, UUID> {
	@Query(value = "select * from inventories i where i.product_id = ?1 ",nativeQuery = true)
	Inventory findByProductid(UUID uuid);
}
