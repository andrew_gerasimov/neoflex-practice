package com.practice.data.repository;

import com.practice.data.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {
	@Query(value = "select o from orders o where order_id = ?1 ",nativeQuery = true)
	Order findByOrder_id(UUID uuid);
}
