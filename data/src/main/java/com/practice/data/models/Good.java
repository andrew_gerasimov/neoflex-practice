package com.practice.data.models;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
@DynamicInsert
@DynamicUpdate
@Table(name = "goods")
public class Good {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
					name = "UUID",
					strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column(name = "product_id", updatable = false, nullable = false)
	private UUID product_id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "price", nullable = false)
	private Double price;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "warranty", nullable = false)
	private Date warranty;

	@OneToOne(mappedBy = "good")
	private Inventory inventory;

}
