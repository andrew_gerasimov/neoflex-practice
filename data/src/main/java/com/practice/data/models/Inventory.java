package com.practice.data.models;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@DynamicInsert
@DynamicUpdate
@Table(name = "inventories")
public class Inventory {

	@Id
	@Column(name = "product_id")
	private UUID product_id;

	@MapsId
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id")
	private Good good;

	@Column(name = "available")
	private Integer amount;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "inventory")
	private List<Order> orderList;

	public void setGood(Good good){
		if (good != null){
			good.setProduct_id(product_id);
			this.good = good;
		}
	}

	public void setOrderList(List<Order> orderList) {
		if (orderList != null){
			if (!orderList.isEmpty()){
				this.orderList.clear();
			}
			this.orderList.addAll(orderList);
			for (Order order: orderList){
				order.setInventory(this);
			}
		}
	}

	public void addOrder(Order order){
		order.setInventory(this);
		this.orderList.add(order);
	}
}
