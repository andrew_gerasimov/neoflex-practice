package com.practice.data.models;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@DynamicUpdate
@DynamicInsert
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
					name = "UUID",
					strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column(name = "order_id")
	private UUID order_id;

	@Column(name = "count")
	private Integer count;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "inventory_id")
	private Inventory inventory;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id")
	private Customer customer;

}
