package com.practice.data.models;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@DynamicUpdate
@DynamicInsert
@Table(name = "customers")
public class Customer {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
					name = "UUID",
					strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column(name = "id", updatable = false, nullable = false)
	private UUID customer_id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "surname", nullable = false)
	private String surname;

	@Column(name = "email", unique = true, nullable = false)
	private String email;

	@Column(name = "phone", nullable = false)
	private String phone;

	@Column(name = "password", nullable = false)
	private String password;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
	private List<Order> orders = new ArrayList<>();

	public void setOrders(List<Order> orders) {
		if (orders != null) {
			if (!orders.isEmpty()) {
				this.orders.clear();
			}
			this.orders.addAll(orders);
			for (Order order : orders) {
				order.setCustomer(this);
			}
		}
	}

	public void addOrder(Order order) {
		order.setCustomer(this);
		this.orders.add(order);
	}
}
